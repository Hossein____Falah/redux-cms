import { lazy } from "react";
import { createBrowserRouter } from "react-router-dom";

import Layout from "../Container/Layouts/Layout";

const Users = lazy(() => import('../Pages/Users'));
const Articles = lazy(() => import('../Pages/Articles'));
const Courses = lazy(() => import('../Pages/Courses'));
const Infos = lazy(() => import('../Pages/Infos'));

const router = createBrowserRouter([
    {
        path: '/',
        element: <Layout/>,
        children: [
            { path: '/users', element: <Users/> },
            { path: '/articles', element: <Articles/> },
            { path: '/courses', element: <Courses/> },
            { path: '/infos', element: <Infos/> },
        ]
    }
])

export default router;