import { configureStore } from "@reduxjs/toolkit";

import usersSlice from "../features/users/usersSlice";
import coursesSlice from "../features/courses/coursesSlice";
import articlesSlice from "../features/articles/articlesSlice";

const store = configureStore({ 
    reducer: {
        users: usersSlice,
        courses: coursesSlice,
        articles: articlesSlice
    }
});

export default store;