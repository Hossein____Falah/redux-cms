import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getAllArticles } from "../../Services/Api/articlesApi";

export const fetchArticles = createAsyncThunk(
    'articles/fetchArticles',
    async (url) => {
        const { data } = await getAllArticles(url);
        return data;
    }
)

const articlesSlice = createSlice({
    name: 'articles',
    initialState: [],
    reducers: {},
    extraReducers: builder => {
        builder.addCase(fetchArticles.fulfilled, (state, action) => action.payload);
    }
});

export default articlesSlice.reducer;