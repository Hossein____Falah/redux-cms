import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getAllCourses } from "../../Services/Api/coursesApi";

export const fetchCourses = createAsyncThunk(
    'courses/fetchCourses',
    async (url) => {
        const { data } = await getAllCourses(url);
        return data;
    }
)

const coursesSlice = createSlice({
    name: 'courses',
    initialState: [],
    reducers: {},
    extraReducers: builder => {
        builder.addCase(fetchCourses.fulfilled, (state, action) => action.payload);
    }
});

export default coursesSlice.reducer;