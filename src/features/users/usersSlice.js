import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { deleteUser, getAllUsers } from "../../Services/Api/usersApi";

export const fetchUsers = createAsyncThunk(
    'users/fetchUsers',
    async (url) => {
        const { data } = await getAllUsers(url);
        return data;
    }
);

export const removeUser = createAsyncThunk(
    'users/removeUser',
    async (id) => {
        const { data } = await deleteUser(id);
        return data;
    }
)

const usersSlice = createSlice({
    name: 'users',
    initialState: [],
    reducers: {},
    extraReducers: builder => {
        builder.addCase(fetchUsers.fulfilled, (state, action) => action.payload);
        builder.addCase(removeUser.fulfilled, (state, action) => {
        })
    }
});

export default usersSlice.reducer;