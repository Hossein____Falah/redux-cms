import { Suspense } from "react";
import { Link, Outlet } from "react-router-dom";

import { Box, Container, Tab, Tabs } from "@mui/material";
import { Group, Help, Movie, Article } from "@mui/icons-material";
import Grid from '@mui/material/Unstable_Grid2';

import { grey } from "@mui/material/colors";

import { Header, Loading, Sidebar } from "../../Common"

const Layout = () => {
    return (
        <Container maxWidth={"xl"}>
            <Header/>
            <Grid container sx={{ mt: 5 }}>
                <Grid xs={3}>
                    <Sidebar/>
                </Grid>
                <Grid xs={9}>
                    <Box sx={{ background: grey[800], boxShadow: 5, borderRadius: 2 }}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                            <Tabs aria-label="basic tabs example">
                                <Link to={`/users`} style={{ color: 'inherit' }}>
                                    <Tab icon={<Group/>} iconPosition="start" label="کاربران" />
                                </Link>
                                <Link to={`/infos`} style={{ color: 'inherit' }}>
                                    <Tab icon={<Help/>} iconPosition="start" label="اطلاعات" />
                                </Link>
                                <Link to={`/courses`} style={{ color: 'inherit' }}>
                                    <Tab icon={<Movie/>} iconPosition="start" label="دوره ها" />
                                </Link>
                                <Link to={`/articles`} style={{ color: 'inherit' }}>
                                    <Tab icon={<Article/>} iconPosition="start" label="وبلاگ" />
                                </Link>
                            </Tabs>
                        </Box>
                        <Suspense fallback={<Loading/>}>
                            <Box sx={{ p: 3 }}>
                                <Outlet/>
                            </Box>
                        </Suspense>
                    </Box>
                </Grid>
            </Grid>
        </Container>
    )
}

export default Layout