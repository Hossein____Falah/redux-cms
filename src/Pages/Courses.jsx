import { Box, Button, Stack } from "@mui/material"

import { CourseBox } from "../Components"
import { useDispatch, useSelector } from "react-redux"
import { useEffect } from "react";
import { fetchCourses } from "../features/courses/coursesSlice";

const Courses = () => {
    const dispatch = useDispatch();

    const courses = useSelector(state => state.courses);
    console.log(courses);

    useEffect(() => {
        dispatch(fetchCourses('/courses'));
    }, []);

    return (
        <Box>
            {
                courses.length && courses.map(course => (
                    <CourseBox key={course.id} {...course}/>
                ))
            }
            <Stack 
                direction={"row"} 
                justifyContent={"space-between"} 
                alignItems={"center"}
                mt={5}
            >
                <Stack direction={"row"} gap={2}>
                    <Button variant="contained" color="info" sx={{ boxShadow: 4.5 }}>افزودن دوره جدید</Button>
                    <Button variant="contained" color="error" sx={{ boxShadow: 4.5 }}>اعمال تخفیف همه دوره ها</Button>
                </Stack>
                <Button variant="contained" color="success" sx={{ boxShadow: 4.5 }}>افزودن دسته بندی</Button>
            </Stack>
        </Box>
    )
}

export default Courses;