import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"

import { Box, Button, Stack } from "@mui/material"
import { ArticleBox } from "../Components"
import { fetchArticles } from "../features/articles/articlesSlice";

const Articles = () => {
    const dispatch = useDispatch();

    const articles = useSelector(state => state.articles);
    
    useEffect(() => {
        dispatch(fetchArticles('/articles'));
    }, []);

    return (
        <Box>
            {
                articles.length && articles.map(article => (
                    <ArticleBox key={article.id} {...article}/>
                ))
            }
            <Stack 
                direction={"row"} 
                mt={5}
            >
                <Button variant="contained" color="success" sx={{ boxShadow: 4.5 }}>افزودن مقاله جدید</Button>
            </Stack>
        </Box>
    )
}

export default Articles