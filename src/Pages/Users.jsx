import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { 
    Box,
    Button,
    FormControl,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput
} from "@mui/material";

import { Delete, Search } from "@mui/icons-material";

import { fetchUsers } from "../features/users/usersSlice";

import { UserItem } from "../Components";

const Users = () => {
    const users = useSelector(state => state.users);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUsers('/users'));
    }, []);

    return (
        <Box>
            <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <FormControl sx={{ width: '45%' }} variant="outlined">
                    <InputLabel 
                        htmlFor="outlined-adornment-Search" 
                        color="warning"
                        sx={{ wordSpacing: 2 }}>نام یا ایمیل کاربر را وارد کنید</InputLabel>
                    <OutlinedInput
                        id="outlined-adornment-Search"
                        type={'text'}
                        endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                            aria-label="toggle Search visibility"
                            edge="end"
                            >
                                <Search />
                            </IconButton>
                        </InputAdornment>
                        }
                        label="Search"
                    />
                </FormControl>
                <Button startIcon={<Delete/>} variant="contained" color="error" size="large">حذف کاربر</Button>
            </Box>
            {
                users.map(user => (
                    <UserItem key={user.id} {...user}/>
                ))
            }
        </Box>
    )
}

export default Users