import { Box, Button, Divider, TextField, Typography } from "@mui/material"
import Grid from "@mui/material/Unstable_Grid2"
import { blue, grey } from "@mui/material/colors"

const Infos = () => {
    return (
        <Box sx={{ border: 1, borderColor: grey[600], borderRadius: 2 }}>
            <Typography sx={{ fontSize: 20, p: 1.5 }}>اطلاعات شما</Typography>
            <Divider/>
            <Box component={'form'} sx={{ p: 2 }}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="firstName"
                            name="firstName"
                            label="نام"
                            fullWidth
                            autoComplete="given-name"
                            variant="filled"
                            color="error"
                            
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="lastName"
                            name="lastName"
                            label="نام خانوادگی"
                            fullWidth
                            autoComplete="family-name"
                            variant="filled"
                            color="error"
                            
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="city"
                            name="city"
                            label="نام کاربری"
                            fullWidth
                            autoComplete="shipping address-level2"
                            variant="filled"
                            color="error"
                            
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="state"
                            name="state"
                            label="ایمیل"
                            fullWidth
                            variant="filled"
                            color="error"
                            
                        />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <TextField
                            required
                            id="zip"
                            name="zip"
                            label="رمز فعلی"
                            fullWidth
                            autoComplete="shipping postal-code"
                            variant="filled"
                            color="error"
                            
                        />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <TextField
                            required
                            id="country"
                            name="country"
                            label="رمز جدید"
                            fullWidth
                            autoComplete="shipping country"
                            variant="filled"
                            color="error"
                            
                        />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <TextField
                            required
                            id="country"
                            name="country"
                            label="تکرار رمز جدید"
                            fullWidth
                            autoComplete="shipping country"
                            variant="filled"
                            color="error"
                            sx={{ wordSpacing: 3 }}
                        />
                    </Grid>
                    <Grid item xs={12} sm={3}>
                        <Box sx={{ border: 2, borderColor: grey[600], p: 2 }}>                            
                            <Box 
                                component={'img'}
                                src="/Images/profile.jpg"
                                sx={{ width: 1, height: 350, objectFit: 'cover'}}
                                alt="upload profile"
                            />
                        </Box>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <Box sx={{ border: 2, borderColor: grey[600], p: 2 }}>                            
                            <Box 
                                component={'img'}
                                src="/Images/wallpaper.jpg"
                                sx={{ width: 1, height: 350, objectFit: 'cover'}}
                                alt="wallpaper"
                            />
                        </Box>
                    </Grid>
                    <Grid item xs={12} sm={5}>
                        <Button 
                            variant="contained" 
                            color="info" 
                            sx={{ 
                                background: blue[700], 
                                color: grey[50],
                                wordSpacing: 3, width: 1
                            }}>
                                اپدیت اطلاعات
                        </Button>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    )
}

export default Infos