import apiRequest from "../Configs/Config";

// @desc Get All Courses
// @route GET https://localhost:9000/courses
const getAllCourses = (url) => {
    return apiRequest.get(url);
};

export {
    getAllCourses
}