import apiRequest from "../Configs/Config";

// @desc Get All Users
// @route GET https://localhost:9000/users
const getAllUsers = (url) => {
    return apiRequest.get(url);
}

const deleteUser = (id) => {
    return apiRequest.delete(id);
}

export {
    getAllUsers,
    deleteUser
}