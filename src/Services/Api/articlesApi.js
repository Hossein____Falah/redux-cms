import apiRequest from "../Configs/Config";

// @desc Get All Articles
// @route GET https://localhost:9000/articles
const getAllArticles = (url) => {
    return apiRequest.get(url);
};

export {
    getAllArticles
}