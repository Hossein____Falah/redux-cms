import { Box, Button, Card, CardContent, CardMedia, Stack, Typography } from "@mui/material";

import PaymentIcon from '@mui/icons-material/Payment';
import FolderIcon from '@mui/icons-material/Folder';
import GroupsIcon from '@mui/icons-material/Groups';

import { grey } from '@mui/material/colors';

const CourseBox = ({ title, price, category, registersCount, image, desc }) => {
    return (
        <Card sx={{ display: 'flex', mb: 2 }}>
            <CardMedia
                component="img"
                sx={{ width: 300 }}
                src={image}
                alt="Nodejs Courses"
            />
            <Box sx={{ display: 'flex', flexDirection: 'column', width: 1 }}>
                <CardContent sx={{ flex: '1 0 auto' }}>
                    <Typography component="p" variant="h5">{title}</Typography>
                    <Typography variant="subtitle1" color="text.secondary" component="p">{desc}</Typography>
                </CardContent>
                <Box 
                    sx={{ 
                        display: 'flex', alignItems: 'center',
                        justifyContent: 'space-between', p: 1, 
                        background: grey[900]
                    }}
                >
                    <Stack direction={"row"} gap={2}>
                        <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
                            <PaymentIcon/>
                            <Typography>قیمت : {price === 0 ? "رایگان" : price.toLocaleString()}</Typography>
                        </Box>
                        <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
                            <FolderIcon/>
                            <Typography>دسته بندی : {category}</Typography>
                        </Box>
                        <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
                            <GroupsIcon/>
                            <Typography>تعداد فروش : {registersCount}</Typography>
                        </Box>
                    </Stack>
                    <Stack direction={'row'} gap={2}>
                        <Button variant="outlined" color="error">حذف</Button>
                        <Button variant="outlined" color="warning">ویرایش</Button>
                    </Stack>
                </Box>
            </Box>
        </Card>
    )
}

export default CourseBox;