import { Box, Button, Card, CardContent, CardMedia, Stack, Typography } from "@mui/material"

import { Category, Groups,  } from '@mui/icons-material';

import { grey } from "@mui/material/colors";

const ArticleBox = ({ title, views, category, image, desc }) => {
    return (
        <Card sx={{ display: 'flex', boxShadow: 5, mb: 3 }}>
            <CardMedia
                component="img"
                sx={{ width: 300 }}
                src={image}
                alt={title}
            />
            <Box sx={{ display: 'flex', flexDirection: 'column', width: 1 }}>
                <CardContent sx={{ flex: '1 0 auto' }}>
                    <Typography component="p" variant="h5" mb={2}>{title}</Typography>
                    <Typography variant="subtitle1" color="text.secondary" component="p">{desc}</Typography>
                </CardContent>
                <Box 
                    sx={{ 
                        display: 'flex', alignItems: 'center',
                        justifyContent: 'space-between', p: 1, 
                        background: grey[900]
                    }}
                >
                    <Stack direction={"row"} gap={2}>
                        <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
                            <Category/>
                            <Typography>دسته بندی : {category}</Typography>
                        </Box>
                        <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
                            <Groups/>
                            <Typography>تعداد بازدید : {views}</Typography>
                        </Box>
                    </Stack>
                    <Stack direction={'row'} gap={2}>
                        <Button variant="outlined" color="error">حذف</Button>
                        <Button variant="outlined" color="warning">ویرایش</Button>
                    </Stack>
                </Box>
            </Box>
        </Card>
    )
}

export default ArticleBox