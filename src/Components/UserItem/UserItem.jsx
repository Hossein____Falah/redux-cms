import { forwardRef, useState } from "react";
import { useDispatch } from "react-redux"
import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Slide, Stack, Typography } from "@mui/material"

import { removeUser } from "../../features/users/usersSlice";

const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

const UserItem = ({ id, image, firstName, lastName, email }) => {
    const dispatch = useDispatch();

    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    
    const handleClose = () => {
        setOpen(false);
    };

    const handleRemove = () => {
        dispatch(removeUser(`/users/${id}`));
        handleClose();
    }

    return (
        <Box 
            sx={{ 
                border: 1, 
                borderColor: "gray", 
                mt: 2, p: 1, display: 'flex', 
                alignItems: 'center', justifyContent: 'space-between',
                boxShadow: 4
            }}
        >
            <Box sx={{ display: 'flex', alignItems: 'center', gap: 2 }}>
                <Box 
                    component="img" 
                    sx={{ 
                        width: 50, 
                        height: 50, 
                        objectFit: 'cover'
                    }} 
                    src={image} 
                    alt="" 
                    loading="lazy"
                />
                <Box sx={{ display: 'flex', flexDirection: 'column', gap: 0.5 }}>
                    <Typography>{firstName} {lastName}</Typography>
                    <Typography>{email}</Typography>
                </Box>
            </Box>
            <Stack direction={"row"} gap={2}>
                <Button 
                    variant="contained" 
                    color="warning"
                >
                    پیام ها
                </Button>
                <Button 
                    variant="contained" 
                    color="info"
                >
                    اطلاعات
                </Button>
                <Button 
                    variant="contained" 
                    color="error"
                    onClick={handleClickOpen}
                >
                    حذف
                </Button>
            </Stack>

            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle>{"آیا از حذف مطمعن هستید؟"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        این عملیات غیرقابل بازگشت می باشد
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button variant="outlined" color="warning" onClick={handleClose}>خیر</Button>
                    <Button variant="outlined" color="error" onClick={handleRemove}>بله</Button>
                </DialogActions>
            </Dialog>
        </Box>
    )
}

export default UserItem