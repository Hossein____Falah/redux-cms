import { createTheme } from "@mui/material";
import { grey } from "@mui/material/colors";

export const theme = createTheme({
    palette: {
        mode: 'dark',
        background: {
            default: grey[900]
        },
        primary: {
            main: grey[800]
        }
    },
    direction: 'rtl',
    typography: {
        fontFamily: 'YekanBakh'
    }
});