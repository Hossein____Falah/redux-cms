import { createRoot } from 'react-dom/client'
import { RouterProvider } from 'react-router-dom'
import { Provider } from "react-redux";

import { CssBaseline, ThemeProvider } from '@mui/material';
import { CacheProvider } from '@emotion/react';
import rtlPlugin from 'stylis-plugin-rtl';
import createCache from "@emotion/cache";
import { prefixer } from 'stylis';

import { theme } from './Theme/Theme';
import router from './Routes/router'

import './Styles/fonts.css';
import './Styles/index.css';
import store from './App/store';

// Create Rtl cache

const cacheRtl = createCache({
  key: 'muirtl',
  stylisPlugins: [prefixer, rtlPlugin]
});

createRoot(document.getElementById('root')).render(
    <Provider store={store}>
      <CacheProvider value={cacheRtl}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <RouterProvider router={router}/>
        </ThemeProvider>
      </CacheProvider>
    </Provider>
)
