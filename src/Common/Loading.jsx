import { CircularProgress } from "@mui/material"

const Loading = () => {
    return <CircularProgress 
        color="warning" 
        sx={{ 
            width: 100, 
            height: 100, 
            display: 'flex', 
            alignItems: 'center', 
            justifyContent: 'center' 
        }} />
}

export default Loading