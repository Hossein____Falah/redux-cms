import { Avatar, Badge, Box, Button, Stack, Typography } from "@mui/material";
import NotificationsIcon from '@mui/icons-material/Notifications';
import LogoutIcon from '@mui/icons-material/Logout';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import { theme } from "../Theme/Theme";
import { blue, yellow } from "@mui/material/colors";

const Header = () => {
    return (
        <Box
            sx={{ 
                background: theme.palette.primary.main, 
                mt: 5,
                p: 2,
                borderRadius: 3,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                boxShadow: 6,
            }}>
            <Box sx={{ display: 'flex', alignItems: 'center', gap: 2 }}>
                <Avatar 
                    alt="HF" 
                    src="/Images/profile.jpg"
                    sx={{ width: 75, height: 75, objectFit: "cover" }}/>
                <Box sx={{ display: 'flex', flexDirection: 'column', gap: 1 }}>
                    <Typography sx={{ fontSize: 22, fontWeight: 700 }} >حسین فلاح</Typography>
                    <Typography>Front-End Developer</Typography>
                </Box>
            </Box>
            <Stack direction={"row"} gap={3}>
                <Button 
                    variant="contained"
                    sx={{ background: yellow[700], ":hover": { background: yellow[800] } }}>
                    <WbSunnyIcon/>
                </Button>
                <Badge 
                    badgeContent={4} 
                    color="error"
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'left',
                        }}>
                    <Button variant="contained" color="success" sx={{ color: '#fff' }}>
                        <NotificationsIcon/>
                    </Button>
                </Badge>
                <Button
                    variant="contained" 
                    sx={{ background: blue[700], ":hover": { background: blue[800] }}}
                    startIcon={<LogoutIcon/>}>
                        خروج از پنل
                </Button>
            </Stack>
        </Box>
    )
}

export default Header