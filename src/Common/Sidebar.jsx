import { Box, Button, Card, CardActions, CardContent, CardMedia, Divider, Stack, Typography } from "@mui/material"
import { blue, grey } from "@mui/material/colors"

import PersonIcon from '@mui/icons-material/Person';
import InfoIcon from '@mui/icons-material/Info';
import EditIcon from '@mui/icons-material/Edit';

const Sidebar = () => {
    return (
        <Card sx={{ maxWidth: 350, background: grey[800], position: "relative" }}>
            <CardMedia
                sx={{ height: 200 }}
                image="/Images/wallpaper.jpg"
                title="wallpaper profile"
            />
            <CardContent>
                <Typography gutterBottom variant="h5" sx={{ fontWeight: 700, textAlign: 'center', mt: 3 }}>
                    حسین فلاح
                </Typography>
                <Typography gutterBottom variant="h6" sx={{ textAlign: 'center' }}>
                    Software Engineer
                </Typography>
                <Box 
                    component="img" 
                    src="/Images/profile.jpg" 
                    sx={{ 
                        position: 'absolute', 
                        top: '30%',
                        left: '38%',
                        width: 80, 
                        height: 80,
                        objectFit: 'cover',
                        borderRadius: 3,
                        boxShadow: 8
                    }} 
                    alt="profile"
                />
                <Stack gap={1}>
                    <Divider/>
                    <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
                            <PersonIcon/>
                            <Typography>نام</Typography>
                        </Box>
                        <Typography>حسین</Typography>
                    </Box>
                    <Divider/>
                    <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
                            <PersonIcon/>
                            <Typography>نام خانوادگی</Typography>
                        </Box>
                        <Typography>فلاح</Typography>
                    </Box>
                    <Divider/>
                    <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
                            <InfoIcon/>
                            <Typography>تعداد دوره</Typography>
                        </Box>
                        <Typography>6</Typography>
                    </Box>
                </Stack>
            </CardContent>
            <CardActions>
                <Button 
                    variant="contained" 
                    sx={{ 
                        width:1, 
                        boxShadow: 6, 
                        background: blue[700], ":hover": { 
                            background: blue[800]
                        }, 
                        wordSpacing: 5
                    }}
                    endIcon={<EditIcon/>}>
                    تغییر اطلاعات
                </Button>
            </CardActions>
        </Card>
    )
}

export default Sidebar